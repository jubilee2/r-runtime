FROM r-base:4.1.2

# set timezone = America/Chicago
ENV TZ=America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# install system dependencies
RUN apt-get update && \
    apt-get install -y default-libmysqlclient-dev libcurl4 nodejs \
		libxml2-dev \
		libcurl4-openssl-dev \
		pandoc \
		libharfbuzz-dev \
		libfribidi-dev \
		libfreetype6-dev \
		libpng-dev \
		libtiff5-dev \
		libjpeg-dev \
		libpq-dev \
		libv8-dev \
		libsodium-dev \
		python3 \
		python3-pip \
		cron \
		npm \
		openssh-client \
		git \
		libfontconfig1-dev \
		lmodern \
		texlive-xetex \
	&& rm -rf /var/lib/apt/lists/*

RUN tlmgr init-usertree

RUN pip3 install pyyaml python-crontab requests
